DEFINES="-DFRAMEBUFFER_WIDTH=144 -DFRAMEBUFFER_HEIGHT=144"
SRC=(
  main.c 
  ../src/boing.c 
  ../src/ball.c 
  ../src/grid.c 
  ../src/miniGL/draw2d.c 
  ../src/miniGL/miniGL.c 
  )

gcc $DEFINES -framework Cocoa -g -std=c99 -I. -I../src ${SRC[@]} SDLMain.m -o sdl_main.bin -lm -lSDL
