#include "miniGL/miniGL.h"

#define RED 63
#define WHITE 191

#define RADIUS 30
#define SLICES 12
#define STACKS 6

#define SLICE_STEP 2 * PI / SLICES
#define STACK_STEP PI / STACKS

typedef struct {
  sll x;
  sll y;
  sll z;
} vertex;

void draw_ball(void);
