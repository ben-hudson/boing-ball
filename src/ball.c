#include "miniGL/miniGL.h"

#include "ball.h"

void draw_ball(void) {
  glPolygonMode(GL_FRONT, GL_FILL);

  vertex ne;
  vertex nw;
  vertex sw;
  vertex se;
  sll nr, sr;

  int color = 0;

  int stack, slice;
  for(stack = 0; stack < STACKS; stack++) {
    ne.y = nw.y = sllmul(sllcos(dbl2sll((stack + 1) * STACK_STEP)), int2sll(RADIUS));
    se.y = sw.y = sllmul(sllcos(dbl2sll(stack * STACK_STEP)), int2sll(RADIUS));

    nr = sllmul(sllsin(dbl2sll((stack + 1) * STACK_STEP)), int2sll(RADIUS));
    sr = sllmul(sllsin(dbl2sll(stack * STACK_STEP)), int2sll(RADIUS));

    for(slice = 0; slice < SLICES; slice++) {
      if(color) {
        glColor3i(WHITE, WHITE, WHITE);
      } else {
        glColor3i(RED, RED, RED);
      }
      color = !color;

      nw.x = sllmul(sllcos(dbl2sll(slice * SLICE_STEP)), nr);
      ne.x = sllmul(sllcos(dbl2sll((slice + 1) * SLICE_STEP)), nr);
      sw.x = sllmul(sllcos(dbl2sll(slice * SLICE_STEP)), sr);
      se.x = sllmul(sllcos(dbl2sll((slice + 1) * SLICE_STEP)), sr);

      nw.z = sllmul(sllsin(dbl2sll(slice * SLICE_STEP)), nr);
      ne.z = sllmul(sllsin(dbl2sll((slice + 1) * SLICE_STEP)), nr);
      sw.z = sllmul(sllsin(dbl2sll(slice * SLICE_STEP)), sr);
      se.z = sllmul(sllsin(dbl2sll((slice + 1) * SLICE_STEP)), sr);

      glBegin(GL_POLYGON);
      glVertex3f(ne.x, ne.y, ne.z);
      glVertex3f(nw.x, nw.y, nw.z);
      glVertex3f(sw.x, sw.y, sw.z);
      glVertex3f(se.x, se.y, se.z);
      glEnd();
    }

    color = !color;
  }

}
