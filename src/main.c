#include <pebble.h>

#include "boing.h"

static Window *window;
static Layer *render_layer;

extern uint8_t *screen_buffer;

static void timer_callback(void *data) {
  layer_mark_dirty(render_layer);
}

static void render_callback(Layer *layer, GContext *ctx) {
  GBitmap *bitmap = (GBitmap *)ctx;
  screen_buffer = (uint8_t*)bitmap->addr;

  gl_drawframe();

  app_timer_register(10, timer_callback, NULL);
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  render_layer = layer_create(bounds);
  layer_set_update_proc(render_layer, render_callback);
  layer_add_child(window_layer, render_layer);
}

static void window_unload(Window *window) {
  layer_destroy(render_layer);
}

static void init(void) {
  window = window_create();
  window_set_fullscreen(window, true);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  window_stack_push(window, true);

  gl_init();
}

static void deinit(void) {
  window_destroy(window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
