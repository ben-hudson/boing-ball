#include "miniGL/miniGL.h"

#include "boing.h"
#include "ball.h"
#include "grid.h"

int abs(int x) {
  return x < 0 ? -x : x;
}

int ix, iy, x, y, r;

void gl_init(void) {
  glViewport(0, 0,
             FRAMEBUFFER_WIDTH,
             FRAMEBUFFER_HEIGHT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glOrtho(int2sll(-XX),
          int2sll(XX),
          int2sll(-YY),
          int2sll(YY),
          int2sll(Z_NEAR),
          int2sll(Z_FAR));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glDisable(GL_LIGHTING);
  glDisable(GL_CULL_FACE);

  ix = iy = 0;
}

void gl_drawframe(void) {
  glClearColor(int2sll(0), int2sll(0), int2sll(0), int2sll(0));
  glClear(GL_COLOR);

  glLoadIdentity();

  glTranslatef(int2sll(-COLS * COL_STEP / 2),
               int2sll(-ROWS * ROW_STEP / 2),
               int2sll(-Z_FAR));
  draw_grid();

  glLoadIdentity();

  glRotatef(int2sll(r), int2sll(0), int2sll(1), int2sll(0));
  glRotatef(int2sll(-15), int2sll(0), int2sll(0), int2sll(1));
  glTranslatef(int2sll(x), int2sll(y - 15), int2sll(0));
  draw_ball();

  ix = (ix + 1) % FRAMEBUFFER_WIDTH;
  iy = (iy + 1) % 16;
  x = abs(ix - FRAMEBUFFER_WIDTH/2) - FRAMEBUFFER_WIDTH/4;
  y = -(iy - 8) * (iy - 8) + FRAMEBUFFER_HEIGHT/2 - RADIUS;
  r = (r + 10) % 360;
}
