#include "miniGL/miniGL.h"

#include "grid.h"

void draw_grid(void) {
  glPolygonMode(GL_FRONT, GL_LINE);

  glColor3i(255, 255, 255);

  int cols, rows;
  for(cols = 0; cols < COLS; cols++) {
    glBegin(GL_POLYGON);
    glVertex3f(int2sll((cols + 1) * COL_STEP), int2sll(ROWS * ROW_STEP), int2sll(0));
    glVertex3f(int2sll(cols * COL_STEP), int2sll(ROWS * ROW_STEP), int2sll(0));
    glVertex3f(int2sll(cols * COL_STEP), int2sll(0), int2sll(0));
    glVertex3f(int2sll((cols + 1) * COL_STEP), int2sll(0), int2sll(0));
    glEnd();
  }

  for(rows = 0; rows < ROWS; rows++) {
    glBegin(GL_POLYGON);
    glVertex3f(int2sll(COLS * COL_STEP), int2sll((rows + 1) * ROW_STEP), int2sll(0));
    glVertex3f(int2sll(COLS * COL_STEP), int2sll(rows * ROW_STEP), int2sll(0));
    glVertex3f(int2sll(0), int2sll(rows * ROW_STEP), int2sll(0));
    glVertex3f(int2sll(0), int2sll((rows + 1) * ROW_STEP), int2sll(0));
    glEnd();
  }

  /*
   * here ends the normal grid drawing
   * the following code adds the pseudo-3D effect
   */

  for(cols = 0; cols < COLS; cols++) {
    glBegin(GL_POLYGON);
    glVertex3f(int2sll((cols + 1) * COL_STEP), int2sll(0), int2sll(0));
    glVertex3f(int2sll(cols * COL_STEP), int2sll(0), int2sll(0));
    glVertex3f(dbl2sll(cols * COL_STEP * 3/2 - COL_STEP * COLS * 1/4), int2sll(-COL_STEP), int2sll(0));
    glVertex3f(dbl2sll((cols + 1) * COL_STEP * 3/2 - COL_STEP * COLS * 1/4), int2sll(-COL_STEP), int2sll(0));
    glEnd();
  }

  glBegin(GL_POLYGON);
  glVertex3f(int2sll(-COL_STEP * 3/4), int2sll(-COL_STEP/2), int2sll(0));
  glVertex3f(int2sll(-COL_STEP * 3/4), int2sll(-COL_STEP/2), int2sll(0));
  glVertex3f(dbl2sll(COLS * COL_STEP + COL_STEP * 3/4), int2sll(-COL_STEP/2), int2sll(0));
  glVertex3f(dbl2sll(COLS * COL_STEP + COL_STEP * 3/4), int2sll(-COL_STEP/2), int2sll(0));
  glEnd();

}
