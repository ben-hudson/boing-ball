#include "miniGL/miniGL.h"

#define XX FRAMEBUFFER_WIDTH / 2
#define YY FRAMEBUFFER_HEIGHT / 2
#define Z_NEAR (XX > YY ? XX : YY) * -2
#define Z_FAR 100

void gl_init(void);
void gl_drawframe(void);
